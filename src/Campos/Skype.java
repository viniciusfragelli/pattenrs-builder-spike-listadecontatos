/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Campos;

/**
 *
 * @author Vinicius
 */
public class Skype extends Campo{

    @Override
    public String getNomeCampo() {
        return "Skype";
    }

    @Override
    public void setCampo(String campo) {
        this.campo = campo;
    }

    @Override
    public String getCampo() {
        return campo;
    }
    
}
