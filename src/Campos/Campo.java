/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Campos;



public abstract class Campo {
    
    protected String campo;
    
    public abstract String getNomeCampo();
    
    public abstract void setCampo(String campo);
    
    public abstract String getCampo();
    
}
