/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Pricipal;

import Campos.Campo;
import Funcao.Funcao;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vinicius
 */
public class CarregaFuncoes {

    public CarregaFuncoes() {
    }
    
    public List<Funcao> getListFuncao() throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        
        List<Funcao> list = new ArrayList<Funcao>();
        Funcao cam;
        File dir = new File("build\\classes\\Funcao\\");
        for(File f : dir.listFiles()){
            if(f.getName().endsWith(".class")){
                //System.out.println(f.getName());
                    String name=f.getName().substring(0,f.getName().length()-6);
                    //System.out.println("nome "+name);
                    Class klass = Class.forName("Funcao."+name);
                    if(!klass.getSimpleName().equals("Funcao"))
                    {
                        cam = (Funcao) klass.newInstance();
                        list.add(cam);
                    }
            }

        }
        return list;
    }
}
