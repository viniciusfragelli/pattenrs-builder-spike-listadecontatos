/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Pricipal;

import Contatos.Contato;
import Contatos.ListContato;
import Funcao.Funcao;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Vinicius
 */
public class Principal {
    
    private List<Funcao> listfunc = new ArrayList<Funcao>();
    
    public Principal() {
        //System.out.println(Principal.class.getName());
        Scanner s = new Scanner(System.in);
        CarregaFuncoes cf = new CarregaFuncoes();
        try{
            listfunc = cf.getListFuncao();
        }catch(Exception e){
            System.out.println("Error 1");
            System.exit(1);
        }
        while(true){
            //System.out.println("\n");
            System.out.println("-----Menu----");
            for (int i = 0; i < listfunc.size(); i++) {
                System.out.println((i+1)+" - "+listfunc.get(i).getNomeFuncao());
            }
            int t = s.nextInt() - 1;
            //System.out.println("\n");
            if(t >= 0 && t < listfunc.size()){
                listfunc.get(t).funcaoAtiva();
            }else{
                System.out.println("Opção invalida!");
            }
        }
    }
    
}
