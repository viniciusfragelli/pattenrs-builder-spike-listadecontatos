/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Contatos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vinicius
 */
public class ListContato {
    
    private static ListContato instance;
    private List<Contato> list = new ArrayList<Contato>();

    public ListContato() {
    }
    
    public static ListContato getInstance(){
        if(instance ==  null)instance = new ListContato();
        return instance;
    }
    
    public List<Contato> getList(){
        return list;
    }
    
}
