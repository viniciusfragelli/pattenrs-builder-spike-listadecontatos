/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Contatos;

import Campos.Campo;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vinicius
 */
public final class Contato {
    
    public final List<Campo> campos;
    private String nome;

    public Contato(String nome) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        CarregaCampos c = new CarregaCampos();
        campos = c.getListCampos();
        this.nome = nome;
    }
    
    public String getNome() {
        return nome;
    }
 
    
    
}
