/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Contatos;

import Campos.Campo;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vinicius
 */
public final class CarregaCampos {

    public CarregaCampos() {
    }
    
    public List<Campo> getListCampos() throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        
        List<Campo> list = new ArrayList<Campo>();
        Campo cam;
        File dir = new File("build\\classes\\Campos\\");
        for(File f : dir.listFiles()){
            if(f.getName().endsWith(".class")){
                //System.out.println(f.getName());
                    String name=f.getName().substring(0,f.getName().length()-6);
                    Class klass = Class.forName("Campos."+name);
                    if(!klass.getSimpleName().equals("Campo"))
                    {
                        cam = (Campo) klass.newInstance();
                        list.add(cam);
                    }
            }

        }
        return list;
    }
    
}
