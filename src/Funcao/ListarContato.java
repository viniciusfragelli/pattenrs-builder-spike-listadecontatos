/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Funcao;

import Contatos.ListContato;

/**
 *
 * @author Vinicius
 */
public class ListarContato extends Funcao{

    @Override
    public void funcaoAtiva() {
        for (int i = 0; i < ListContato.getInstance().getList().size(); i++) {
            System.out.println((i+1)+" - "+ListContato.getInstance().getList().get(i).getNome());
        }
        if(ListContato.getInstance().getList().isEmpty())System.out.println("Sem contatos");
    }

    @Override
    public String getNomeFuncao() {
        return "Lista Contatos";
    }
    
}
