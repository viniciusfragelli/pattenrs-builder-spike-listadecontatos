/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Funcao;

/**
 *
 * @author Vinicius
 */
public class Sair extends Funcao{

    @Override
    public void funcaoAtiva() {
        System.exit(1);
    }

    @Override
    public String getNomeFuncao() {
        return "Sair";
    }
    
}
