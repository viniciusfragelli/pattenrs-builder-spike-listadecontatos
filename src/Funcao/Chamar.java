/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Funcao;

import Contatos.ListContato;
import java.util.Scanner;

/**
 *
 * @author Vinicius
 */
public class Chamar extends Funcao{

    @Override
    public void funcaoAtiva() {
        Scanner s = new Scanner(System.in);
        for (int i = 0; i < ListContato.getInstance().getList().size(); i++) {
                        System.out.println((i+1)+" - "+ListContato.getInstance().getList().get(i).getNome());
        }
        if(ListContato.getInstance().getList().isEmpty())System.out.println("Sem contatos");
        else{
            System.out.println("Selecione um contato:");
            int k = s.nextInt() -1;
            if(ListContato.getInstance().getList().size() > k && k >= 0){
                System.out.println("---Lista de campos---");
                for (int i = 0; i < ListContato.getInstance().getList().get(k).campos.size(); i++) {
                    System.out.println((i+1)+" - "+ListContato.getInstance().getList().get(k).campos.get(i).getNomeCampo()+": "+ListContato.getInstance().getList().get(k).campos.get(i).getCampo());
                }
                System.out.println("Entre com o tipo de chamada: ");
                k = s.nextInt() - 1;
                System.out.println("Discando...");
                System.out.println("Falando...");
                System.out.println("Chamada encerrada...");
            }else{
                System.out.println("Opção invalida!");
            }
        }
    }

    @Override
    public String getNomeFuncao() {
        return "Chamar";
    }
    
}
