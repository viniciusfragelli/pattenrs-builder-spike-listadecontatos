/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Funcao;

import Contatos.ListContato;
import java.util.Scanner;

/**
 *
 * @author Vinicius
 */
public class Deletar extends Funcao{

    @Override
    public void funcaoAtiva() {
        ListarContato c = new ListarContato();
        c.funcaoAtiva();
        System.out.println("Selecione o contato:");
        Scanner s = new Scanner(System.in);
        int k = s.nextInt()-1;
        if(k >= 0 || k < ListContato.getInstance().getList().size()){
            ListContato.getInstance().getList().remove(k);
            System.out.println("Deletado com sucesso!");
        }else{
            System.out.println("Contato não existe!");
        }
    }

    @Override
    public String getNomeFuncao() {
        return "DEletar contato";
    }
    
}
