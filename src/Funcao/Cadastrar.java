/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Funcao;

import Contatos.Contato;
import Contatos.ListContato;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vinicius
 */
public class Cadastrar extends Funcao{

    public Cadastrar() {
    }
 
    @Override
    public void funcaoAtiva() {
        Scanner s = new Scanner(System.in);
        System.out.println("-----Cadastrando novo contato-----");
        System.out.println("---Entre com nome---");
        String nome = s.nextLine();
        try {
            ListContato.getInstance().getList().add(new Contato(nome));
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Cadastrar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Cadastrar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Cadastrar.class.getName()).log(Level.SEVERE, null, ex);
        }
        int size = ListContato.getInstance().getList().size()-1;
        String aux;
        for (int i = 0; i < ListContato.getInstance().getList().get(size).campos.size(); i++) {
            System.out.println("---Entre com o campo "+ListContato.getInstance().getList().get(size).campos.get(i).getNomeCampo()+"---");
            aux = s.next();
            ListContato.getInstance().getList().get(size).campos.get(i).setCampo(aux);
        }
    }

    @Override
    public String getNomeFuncao() {
        return "Cadastrar";
    }
    
}
